package code;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class Cards {
	
	private	ArrayList<Card> _SuspectCards;
	private ArrayList<Card> _WeaponsCards;
	private ArrayList<Card> _RoomCards;
	private ArrayList<Card> _envelope;
	private ArrayList<Card> _restOfTotal;
	
	public Cards() {
		
		Card ColonelMustard = new Card("Colonel Mustard");
		Card MissScarlet = new Card("Miss Scarlet");
		Card ProfessorPlum = new Card("Professor Plum");
		Card MrGreen = new Card("Mr.Green");
		Card MrsWhite = new Card("Mrs.White");
		Card MrPeacock = new Card("Mr.Peacock");

		Card Knife = new Card("Knife");
		Card Candlestick = new Card("Candlestick");
		Card Revolver = new Card("Revolver");
		Card Rope = new Card("Rope");
		Card LeadPipe = new Card("Lead Pipe");
		Card Wrench = new Card("Wrench");

		Card Hall = new Card("Hall");
		Card Lounge = new Card("Lounge");
		Card DiningRoom = new Card("Dining Room");
		Card Kitchen = new Card("Kitchen");
		Card Ballroom = new Card("Ballroom");
		Card Conservatory = new Card("Conservatory");
		Card BillardRoom = new Card("Billard Room");
		Card Library = new Card("Library");
		Card Study = new Card("Study");

		ArrayList<Card> _SuspectCards = new ArrayList<Card>();
		_SuspectCards.add(ColonelMustard);
		_SuspectCards.add(MissScarlet);
		_SuspectCards.add(ProfessorPlum);
		_SuspectCards.add(MrGreen);
		_SuspectCards.add(MrsWhite);
		_SuspectCards.add(MrPeacock);

		ArrayList<Card> _WeaponsCards = new ArrayList<Card>();
		_WeaponsCards.add(Knife);
		_WeaponsCards.add(Candlestick);
		_WeaponsCards.add(Revolver);
		_WeaponsCards.add(Rope);
		_WeaponsCards.add(LeadPipe);
		_WeaponsCards.add(Wrench);

		ArrayList<Card> _RoomCards = new ArrayList<Card>();
		_RoomCards.add(Hall);
		_RoomCards.add(Lounge);
		_RoomCards.add(DiningRoom);
		_RoomCards.add(Kitchen);
		_RoomCards.add(Ballroom);
		_RoomCards.add(Conservatory);
		_RoomCards.add(BillardRoom);
		_RoomCards.add(Library);
		_RoomCards.add(Study);
		
		ArrayList<Card> _envelope = new ArrayList<Card>();
		ArrayList<Card> _restOfTotal = new ArrayList<Card>();
	}
	
	// Take the top card from each group and place it in the envelope. This
			// should be done
			// carefully so that no player knows any of the three cards (one room,
			// one weapon, and
			// one suspect) placed in the envelope.
			// Shuffle together the three piles of remaining cards.
	public ArrayList<Card> getTheAnswer(){
		
		Collections.shuffle(_SuspectCards);
		Collections.shuffle(_WeaponsCards);
		Collections.shuffle(_RoomCards);
		_envelope.add(_SuspectCards.get(0));
		_envelope.add(_WeaponsCards.get(0));
		_envelope.add(_RoomCards.get(0));
		return _envelope;
		
	}
	
	public ArrayList<Card> shuffleTheRest(){
		getTheAnswer();
		_SuspectCards.remove(0);
		_WeaponsCards.remove(0);
		_RoomCards.remove(0);
		_restOfTotal.addAll(_RoomCards);
		_restOfTotal.addAll(_WeaponsCards);
		_restOfTotal.addAll(_SuspectCards);
		Collections.shuffle(_restOfTotal);
		return _restOfTotal;
	}
	
	public void getEachPile3(Player one,Player two,Player three){
		ArrayList<Card> restTotal = new ArrayList<Card>();
		restTotal = shuffleTheRest(); 
		
		ArrayList<Card> cardsOfOne = one.getMyCards();
		ArrayList<Card> cardsOfTwo = two.getMyCards();
		ArrayList<Card> cardsOfThree = three.getMyCards();
		
		for(int i=0;i<18;i++){
			cardsOfOne.add(restTotal.get(i));
			i++;
			cardsOfTwo.add(restTotal.get(i));
			i++;
			cardsOfThree.add(restTotal.get(i));
		}
		one.setMyCards(cardsOfOne);
		two.setMyCards(cardsOfTwo);
		three.setMyCards(cardsOfThree);
		
	}
	public void getEachPile4(Player one,Player two,Player three,Player four){
		ArrayList<Card> restTotal = new ArrayList<Card>();
		restTotal = shuffleTheRest(); 
		
		ArrayList<Card> cardsOfOne = one.getMyCards();
		ArrayList<Card> cardsOfTwo = two.getMyCards();
		ArrayList<Card> cardsOfThree = three.getMyCards();
		ArrayList<Card> cardsOfFour = four.getMyCards();
		
		for(int i=0;i<18;i++){
			cardsOfOne.add(restTotal.get(i));
			i++;
			cardsOfTwo.add(restTotal.get(i));
			i++;
			cardsOfThree.add(restTotal.get(i));
			i++;
			cardsOfFour.add(restTotal.get(i));
		}
		one.setMyCards(cardsOfOne);
		two.setMyCards(cardsOfTwo);
		three.setMyCards(cardsOfThree);
		four.setMyCards(cardsOfFour);
	}
	public void getEachPile5(Player one,Player two,Player three,Player four,Player five){
		ArrayList<Card> restTotal = new ArrayList<Card>();
		restTotal = shuffleTheRest(); 
		
		ArrayList<Card> cardsOfOne = one.getMyCards();
		ArrayList<Card> cardsOfTwo = two.getMyCards();
		ArrayList<Card> cardsOfThree = three.getMyCards();
		ArrayList<Card> cardsOfFour = four.getMyCards();
		ArrayList<Card> cardsOfFive = five.getMyCards();
		
		for(int i=0;i<18;i++){
			cardsOfOne.add(restTotal.get(i));
			i++;
			cardsOfTwo.add(restTotal.get(i));
			i++;
			cardsOfThree.add(restTotal.get(i));
			i++;
			cardsOfFour.add(restTotal.get(i));
			i++;
			cardsOfFive.add(restTotal.get(i));
		}
		one.setMyCards(cardsOfOne);
		two.setMyCards(cardsOfTwo);
		three.setMyCards(cardsOfThree);
		four.setMyCards(cardsOfFour);
		five.setMyCards(cardsOfFive);
	}
	public void getEachPile6(Player one,Player two,Player three,Player four,Player five,Player six){
		ArrayList<Card> restTotal = new ArrayList<Card>();
		restTotal = shuffleTheRest(); 
		
		ArrayList<Card> cardsOfOne = one.getMyCards();
		ArrayList<Card> cardsOfTwo = two.getMyCards();
		ArrayList<Card> cardsOfThree = three.getMyCards();
		ArrayList<Card> cardsOfFour = four.getMyCards();
		ArrayList<Card> cardsOfFive = five.getMyCards();
		ArrayList<Card> cardsOfSix = six.getMyCards();
		
		for(int i=0;i<18;i++){
			cardsOfOne.add(restTotal.get(i));
			i++;
			cardsOfTwo.add(restTotal.get(i));
			i++;
			cardsOfThree.add(restTotal.get(i));
			i++;
			cardsOfFour.add(restTotal.get(i));
			i++;
			cardsOfFive.add(restTotal.get(i));
			i++;
			cardsOfSix.add(restTotal.get(i));
		}
		one.setMyCards(cardsOfOne);
		two.setMyCards(cardsOfTwo);
		three.setMyCards(cardsOfThree);
		four.setMyCards(cardsOfFour);
		five.setMyCards(cardsOfFive);
		six.setMyCards(cardsOfSix);
	}	

}
