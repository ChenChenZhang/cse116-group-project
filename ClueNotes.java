package code;
import java.util.ArrayList;

public class ClueNotes {

	private ArrayList<Note> _notes;
	
	public ClueNotes() {
		Note ColonelMustard = new Note("Colonel Mustard");
		Note MissScarlet = new Note("Miss Scarlet");
		Note ProfessorPlum = new Note("Professor Plum");
		Note MrGreen = new Note("Mr.Green");
		Note MrsWhite = new Note("Mrs.White");
		Note MrPeacock = new Note("Mr.Peacock");

		Note Knife = new Note("Knife");
		Note Candlestick = new Note("Candlestick");
		Note Revolver = new Note("Revolver");
		Note Rope = new Note("Rope");
		Note LeadPipe = new Note("Lead Pipe");
		Note Wrench = new Note("Wrench");

		Note Hall = new Note("Hall");
		Note Lounge = new Note("Lounge");
		Note DiningRoom = new Note("Dining Room");
		Note Kitchen = new Note("Kitchen");
		Note Ballroom = new Note("Ballroom");
		Note Conservatory = new Note("Conservatory");
		Note BillardRoom = new Note("Billard Room");
		Note Library = new Note("Library");
		Note Study = new Note("Study");
		
		ArrayList<Note> _notes = new ArrayList<Note>();
		_notes.add(ColonelMustard);
		_notes.add(MissScarlet);
		_notes.add(ProfessorPlum);
		_notes.add(MrGreen);
		_notes.add(MrsWhite);
		_notes.add(MrPeacock);
		_notes.add(Knife);
		_notes.add(Candlestick);
		_notes.add(Revolver);
		_notes.add(Rope);
		_notes.add(LeadPipe);
		_notes.add(Wrench);
		_notes.add(Hall);
		_notes.add(Lounge);
		_notes.add(DiningRoom);
		_notes.add(Kitchen);
		_notes.add(Ballroom);
		_notes.add(Conservatory);
		_notes.add(BillardRoom);
		_notes.add(Library);
		_notes.add(Study);
	}
}
