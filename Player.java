package code; 

import java.util.ArrayList;

public class Player{
	
	private ArrayList<Card> _myCards;
	private ArrayList<Card> _suggestion;
	private Dice _dice;
	private int _position;
	private int _row;
	private int _column;
	private int _steps;
	private Board _board;
	private int _id;
	private ArrayList<int[]> prePosition;
	
	public Player(Board b, int id) {
	_id = id;
	_board = b;
	_dice = new Dice();
	_myCards = new ArrayList<Card>();
	Cards allCards = new Cards();
	
	_suggestion = new ArrayList<Card>();
	_suggestion.add(0, null);
	_suggestion.add(1, null);
	_suggestion.add(2, null);
	_position = 0;
	_steps = 0;
	
	prePosition = new ArrayList<int[]>();
	}
	public Player(int row, int column){
		_row = row;
		_column = column;
	}
	
	public int rollTheDice() {
		_steps = _dice.shuffle();
		return _steps;
	}
	
	public ArrayList<Card> getMyCards(){
		return _myCards;
	}
	
	public void setMyCards(ArrayList<Card> cards){
		_myCards = cards;
	}
	
	public void setThePosition(int row, int column){
		_board.playerPosition.get(_id)[0] = row;
		_board.playerPosition.get(_id)[1] = column;
		_position = _board._space[row][column];
	}
	
	public void setTheLabel(int label){
		_position = label;
	}
	
	public int getTheRow(){
		return _board.playerPosition.get(_id)[0];
	
	}
	
	public int getTheColumn(){
		
		return _board.playerPosition.get(_id)[1];
	}
	
	public int getPosition(){
		return _position;
	}
	

	//clear previous player's steps. In this way, we can record steps for next player.
	 public void clearPrePosition(){
		prePosition.clear();
	}
	
	public void move(char dir){
		if(dir=='u'){
			if(movable(getTheRow()-1, getTheColumn())){
				this.setThePosition(getTheRow()-1, getTheColumn());
				//record positions that have been walked
				int[] a = {getTheRow()-1, getTheColumn()};
				prePosition.add(a);
				_steps++;
			}
		}
		if(dir=='d'){
			if(movable(getTheRow()+1, getTheColumn())){
			this.setThePosition(getTheRow()+1, getTheColumn());
			
			int[] a = {getTheRow()+1, getTheColumn()};
			prePosition.add(a);
			_steps++;}
			
		}
		if(dir=='l'){
			if(movable(getTheRow(), getTheColumn()-1)){
			 this.setThePosition(getTheRow(), getTheColumn()-1);
			
			int[] a = {getTheRow(), getTheColumn()-1};
			prePosition.add(a);
			_steps++;}

		}
		if(dir=='r'){
			if(movable(getTheRow(), getTheColumn()+1)){
		    this.setThePosition(getTheRow(), getTheColumn()+1);
			
			int[] a = {getTheRow(), getTheColumn()+1};
			prePosition.add(a);
			_steps++;}
	    }
	}
	public boolean movable(int row, int col){
		//check if out of bounds
		if((row<0||row>22)||(col<0||col>22)){return false;}
		//check if there are rooms and hallways
		if(_board._space[row][col]!=0&&_board._space[row][col]<100){
			return false;
		}
		//check if there are players,if one player is in that position,then others cant walk there.
		for(int i=0;i<_board.playerPosition.size();i++){
			if(row==_board.playerPosition.get(i)[0]&&col==_board.playerPosition.get(i)[1]){
				return false;
			}
		}
	//make sure they cant walk backward
		for(int i=0; i<prePosition.size(); ++i){
			if(prePosition.get(i)[0] == row && prePosition.get(i)[1] == col)
				return false;
		}
		
		return true;
	}
	public void makeASuggestion(Card character, Card room, Card weapon){
		
		if(getPosition()!=0||getPosition()!=1){
		_suggestion.set(0, character);
		_suggestion.set(1, weapon);
		_suggestion.set(2, room);
		}
	}
	
	public ArrayList<Card> checkSuggestion(){
		ArrayList<Card> showOutCards = new ArrayList<Card>();
		if(_myCards.contains(_suggestion.get(0))){
			showOutCards.add(_suggestion.get(0));
		}
		if(_myCards.contains(_suggestion.get(1))){
			showOutCards.add(_suggestion.get(1));
		}
		if(_myCards.contains(_suggestion.get(2))){
			showOutCards.add(_suggestion.get(2));
		}
		
		return showOutCards;
	}
	
}
